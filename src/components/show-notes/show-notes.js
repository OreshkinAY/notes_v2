import DragControlBlock from '@/components/drag-control-block/drag-control-block.vue'

export default {
  name: 'Note',
  data () {
    return {
      draggableNode: null,
      showNoteId: null,
      blockControlPath: ''
    }
  },
  props: {
    notes: {},
    status: ''
  },
  computed: {
    notesFiltered () {
      function findNotes (status) {
        return function (element) {
          return element.options.status === status
        }
      }
      return this.notes.filter(findNotes(this.status))
    }
  },
  methods: {
    removeNote: function (note) {
      const indexRemoveNote = this.findProcessedNote(note)
      this.$store.dispatch('removeNote', indexRemoveNote)
    },
    addArchive: function (note) {
      const indexArchivedNote = this.findProcessedNote(note)
      this.$store.dispatch('archivedNote', indexArchivedNote)
    },
    ofArchive: function (note) {
      const indexOfArchivedNote = this.findProcessedNote(note)
      this.$store.dispatch('ofArchivedNote', indexOfArchivedNote)
    },
    findProcessedNote: function (note) {
      function findIndexNote (element, index) {
        return element.uuidv1 === note.uuidv1
      }

      return this.$store.getters.allNotes.findIndex(findIndexNote)
    },

    // DragAndDrop
    onDragStart: function (note, data, event) {
      this.draggableNode = note
    },
    onDragEnter: function (note, data, event) {
      if (this.draggableNode.order !== note.order) {
        this.showNoteId = note.uuidv1
      } else {
        this.showNoteId = false
      }
    },
    dropNote: function (note, data, event) {
      this.showNoteId = false
      const allNotes = this.$store.getters.allNotes
      const indexDragNote = allNotes.indexOf(this.draggableNode)
      const indexDropNote = allNotes.indexOf(note)

      // если соседний индекс равен предыд+1 и блок=left, нет смысла dispatch
      if (this.blockControlPath === 'left' && indexDragNote + 1 === indexDropNote) {
        return
      }

      this.$store.dispatch('movingNote', {
        indexDragNote: indexDragNote,
        indexDropNote: indexDropNote,
        patchControlBlock: this.blockControlPath,
        draggableNode: this.draggableNode})
    },
    dropBlockControlPath: function (dropBlockControlPath) {
      this.blockControlPath = dropBlockControlPath
    }
  },
  components: {
    'drag-control-block': DragControlBlock
  }
}

import Note from '../../models/note'

export default {
  name: 'Note',
  data () {
    return {
      show: false,
      note: Note.createNote()
    }
  },
  methods: {
    checkForm: function (event) {
      if (this.note.name !== undefined && this.note.name.length > 0) {
        this.$store.dispatch('addNote', this.note)
        this.note = Note.createNote()
      }
    },
    Cancel: function (event) {
      Note.reload(this.note)
    }
  }
}

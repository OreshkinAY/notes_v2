export default {
  name: 'Note',
  data () {
    return {
      indexEditNote: this.$store.getters.editNote(this.$route.params.id),
      cachedNote: {},
      colors: [
        {name: 'Белый', value: 'White'},
        {name: 'Зеленый', value: 'Lightgreen'},
        {name: 'Голубой', value: 'Skyblue'}
      ]
    }
  },

  created: function () {
    this.cachedNote = JSON.parse(JSON.stringify(this.$store.getters.allNotes[this.indexEditNote]))
  },

  methods: {
    save: function () {
      this.$store.dispatch('editNote', {index: this.indexEditNote, noteEdit: this.cachedNote})
    },
    shadow: function (event) {
      var target = event.target
      target.classList.toggle('shadow')
    }
  }
}

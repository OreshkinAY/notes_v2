export default {
  name: 'Note',
  data () {
    return {
      draggableNode: null,
      dragControlParts: ['left', 'center', 'raight'],
      dragControlPathCurrent: ''
    }
  },
  props: {
    currentBlock: {},
    showControlBlock: false
  },
  methods: {
    onDragEnterPath: function (dragControlPathCurrent) {
      console.log(dragControlPathCurrent)
      this.dragControlPathCurrent = dragControlPathCurrent
    },
    onDragLeavePath: function (dragControlPathLeave) {
      console.log('leave', dragControlPathLeave)
      console.log('current', this.dragControlPathCurrent)
      if (dragControlPathLeave === this.dragControlPathCurrent) {
        this.dragControlPathCurrent = ''
      }
    },
    onDropPath: function (dragControlPath) {
      this.dragControlPathCurrent = ''
      this.$emit('dropBlockControlPath', dragControlPath)
    }
  }
}

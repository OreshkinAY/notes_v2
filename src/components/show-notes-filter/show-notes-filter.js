export default {
  name: 'Note',
  props: {
    notes: {},
    searchText: '',
    status: ''
  },
  computed: {
    searchNotes () {
      const filterNotes = (searchText, status) => {
        return this.$store.getters.allNotes.filter((note) => {
          if (note.options.status === status) {
            return searchContent(note.name) || searchContent(note.text)
          }
        })

        function searchContent (note) {
          if (typeof note !== 'undefined') {
            return note.toLowerCase().indexOf(searchText.toLowerCase()) > -1
          }
        }
      }
      return filterNotes(this.searchText, this.status)
    }
  },
  methods: {
    removeNote: function (note) {
      const indexRemoveNote = this.findProcessedNote(note)
      this.$store.dispatch('removeNote', indexRemoveNote)
    },
    addArchive: function (note) {
      const indexArchivedNote = this.findProcessedNote(note)
      this.$store.dispatch('archivedNote', indexArchivedNote)
    },
    ofArchive: function (note) {
      const indexOfArchivedNote = this.findProcessedNote(note)
      this.$store.dispatch('ofArchivedNote', indexOfArchivedNote)
    },
    findProcessedNote: function (note) {
      function findIndexNote (element, index) {
        return element.uuidv1 === note.uuidv1
      }
      return this.$store.getters.allNotes.findIndex(findIndexNote)
    }
  }
}

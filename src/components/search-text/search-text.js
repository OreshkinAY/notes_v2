export default {
  name: 'Note',
  data () {
    return {
      searchText: '',
      searchActivate: false
    }
  },

  methods: {
    searchTextInNotes: function (event) {
      this.$emit('searchText', this.searchText)
    }
  }
}

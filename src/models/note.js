class Note {
  static options = {
    name: '',
    text: '',
    options: {
      color: 'white',
      status: 'new',
      list: false
    }
  }
  static optionsStatusNew='new'
  static optionsStatusArchived='archived'

  constructor (uuidv1, {options: {color, status, list} = {}}) {
    this.uuidv1 = uuidv1
    this.options = {status, color, list}
  }

  static createNote () {
    const uuidv1 = require('uuid/v1')()
    return new Note(uuidv1, this.options)
  }

  static reload (Note) {
    Object.assign(Note, this.options)
  }
}

export default Note

import Vue from 'vue'
import Router from 'vue-router'
import MainNotes from '@/components/main-notes/main-notes'
import EditNote from '@/components/edit-note/edit-note.vue'
import NotFoundComponent from '@/components/error-page/error-page.vue'
import store from '../store/index.js'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/',
      name: 'MainNotes',
      component: MainNotes
    },
    {
      path: '/archived',
      name: 'ArchivedNotes',
      component: MainNotes
    },
    {
      path: '/note/:id',
      name: 'EditNote',
      component: EditNote,
      beforeEnter: (to, from, next) => {
        function isValid (param) {
          const editNote = store.getters.editNote(param)
          return editNote === undefined
        }

        if (isValid(to.params.id)) {
          next({name: 'not-found'})
        }
        next()
      }
    },
    {
      path: '*',
      redirect: '/404'
    },
    {
      path: '/404',
      name: 'not-found',
      component: NotFoundComponent
    }
  ]
})

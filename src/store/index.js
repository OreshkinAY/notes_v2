import Vue from 'vue'
import Vuex from 'vuex'
// import Note from '../models/note'

Vue.use(Vuex)

// import NodeClass from '../models/NoteClass'

const loadState = (key) => {
  try {
    const serializedState = localStorage.getItem(key)
    if (serializedState === null) {
      return undefined
    }
    return JSON.parse(serializedState)
  } catch (err) {
    return undefined
  }
}

const saveState = (state, key) => {
  try {
    const serializedState = JSON.stringify(state)
    localStorage.setItem(key, serializedState)
  } catch (err) {
    console.error(`Something went wrong: ${err}`)
  }
}

const store = new Vuex.Store({
  state: {
    notes: loadState('notes') || []
  },
  actions: {
    addNote ({commit}, note) {
      commit('ADD_NOTE', note)
    },
    removeNote ({commit}, indexRemoveNote) {
      commit('REMOVE_NOTE', indexRemoveNote)
    },
    archivedNote ({commit}, indexArchivedNote) {
      commit('ARCHIVED_NOTE', indexArchivedNote)
    },
    ofArchivedNote ({commit}, indexOfArchivedNote) {
      commit('OF_ARCHIVED_NOTE', indexOfArchivedNote)
    },
    editNote ({commit}, cachedNote) {
      commit('EDIT_NOTE', cachedNote)
    },
    movingNote ({commit}, indexDragDropNotes) {
      if (indexDragDropNotes.patchControlBlock === 'center') {
        commit('MOVING_NOTE_CENTER', indexDragDropNotes)
      } else {
        if (indexDragDropNotes.patchControlBlock === 'raight') {
          indexDragDropNotes.indexDropNote += 1
        }
        commit('MOVING_NOTE', indexDragDropNotes)
      }
    }
  },
  mutations: {
    ADD_NOTE (state, note) {
      note.order = state.notes.length + 1
      state.notes.push(note)
      saveState(state.notes, 'notes')
    },
    REMOVE_NOTE (state, indexRemoveNote) {
      state.notes.splice(indexRemoveNote, 1)
      saveState(state.notes, 'notes')
    },
    ARCHIVED_NOTE (state, indexArchivedNote) {
      state.notes[indexArchivedNote].options.status = 'archived'
      saveState(state.notes, 'notes')
    },
    OF_ARCHIVED_NOTE (state, indexOfArchivedNote) {
      state.notes[indexOfArchivedNote].options.status = 'new'
      saveState(state.notes, 'notes')
    },
    EDIT_NOTE (state, cachedNote) {
      state.notes[cachedNote.index] = JSON.parse(JSON.stringify(cachedNote.noteEdit))
      saveState(state.notes, 'notes')
    },
    MOVING_NOTE (state, indexDragDropNotes) {
      state.notes.splice(indexDragDropNotes.indexDropNote, 0, indexDragDropNotes.draggableNode)
      state.notes.splice(indexDragDropNotes.indexDragNote, 1)
    },
    MOVING_NOTE_CENTER (state, indexDragDropNotes) {
      let indexDragNote = indexDragDropNotes.indexDragNote
      let indexDropNote = indexDragDropNotes.indexDropNote
      state.notes[indexDropNote] = state.notes.splice(indexDragNote, 1, state.notes[indexDropNote])[0]
    }
  },
  getters: {
    editNote (state) {
      return function edit (uuidvEditNote) {
        return state.notes.findIndex(note => note.uuidv1 === uuidvEditNote)
      }
    },
    notes: state => status => {
      return state.notes.filter(findNotes)

      function findNotes (element, index) {
        return element.options.status === status
      }
    },
    allNotes (state) {
      return state.notes
    }
  }
})

export default store
